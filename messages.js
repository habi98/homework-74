const express = require('express');
const fs = require('fs');

const fileMs = require('./fileMs');

const router = express.Router();

const messages = './messages';

router.get('/', (req, res) => {
    res.send(fileMs.getItems())
});

router.post('/', (req, res) => {
    const date = new Date().toISOString();
    const message = {
        message: req.body.message,
        date: date
    };
    fs.writeFileSync(`./messages/${date}.txt`, JSON.stringify(message));
    res.send({message: 'OK'})

});


module.exports = router;